definition module Text.YAML

/**
 * This library provides functionality to load YAML 1.2, a "human-friendly,
 * cross-language, Unicode based data serialization language"
 * (https://yaml.org/spec/1.2/spec.html).
 *
 * YAML is loaded in three steps:
 *
 * - A character stream is parsed to an event tree (Text.YAML.Parse)
 * - A node graph is composed from the event tree (Text.YAML.Compose)
 * - A native data structure is constructed from the node graph
 *   (Text.YAML.Construct)
 *
 * The last step is done generically in a way defined in the corresponding
 * module.
 *
 * All these steps are combined in `loadYAML`. Normally, only this module needs
 * to be imported. However, it is possible to run each step individually by
 * using their functions directly from their respective modules.
 *
 * YAML defines several ways in which the types of scalar nodes can be
 * resolved, called schemas (chapter 10 of the specification). The recommended
 * schemas are defined in Text.YAML.Schemas. The parser receives the schema to
 * be used as a parameter, so it is possible to write one's own schema. By
 * default, the `coreSchema` should be used.
 *
 * Currently, the other direction (native data to character stream) is not
 * supported.
 *
 * In Text.YAML.JSON, a function is defined that can convert YAML to JSON.
 *
 * Copyright 2021-2023 Camil Staps.
 * Copyright 2021 Gijs Alberts.
 *
 * This file is part of clean-yaml.
 *
 * Clean-yaml is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Clean-yaml is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * clean-yaml. If not, see <https://www.gnu.org/licenses/>.
 */

from Data.Either import :: Either
from Data.Error import :: MaybeError
from StdOverloaded import class toString

from Text.YAML.Construct import class ConstructFromYAML
from Text.YAML.Schemas import :: YAMLSchema, failsafeSchema, jsonSchema, coreSchema

:: YAMLError
	= IllFormed !String
		//* The character stream was ill-formed during parsing.
	| UnsupportedSyntax !String
		//* The parser encountered syntax that is not supported. In particular,
		//* only YAML 1.2+ is supported.
	| UnrecognizedTag !String
		//* The parser encountered a tag handle that was not defined with a
		//* `TAG` directive.
	| UnidentifiedAlias !String
		//* While composing the node graph, an alias could not be identified.
	| UnresolvedTag
		//* While composing the node graph, the tag of a node with no specified
		//* tag could not be resolved (this may be due to a limitation of the
		//* schema).
	| InvalidContent !String
		//* While constructing the native data structure, the node graph did
		//* not have the expected structure.

instance toString YAMLError

/**
 * A custom error type for `constructFromYAML` and `loadYAML` which provides a
 * stack of locations to indicate the position in the data structure where
 * construction failed.
 */
:: YAMLErrorWithLocations =
	{ error :: !YAMLError //* The error that occurred.
	, locations :: ![ErrorLocation]
		//* Information on where the error is located.
		//* The outermost location is the first element.
	}

//* Lifts a `YAMLError` to a `YAMLErrorWithLocations` without `locations`.
withoutErrorLocations :: !YAMLError -> YAMLErrorWithLocations

/**
 * When no Clean data structure can be constructed from a YAML structure,
 * `YAMLErrorWithLocations` includes a `ErrorLocation` stack to indicate where
 * constructing failed, exactly.
 */
:: ErrorLocation
	= ADT !String
		//* The name of an ADT (the ADT is not reflected in the YAML itself;
		//* see comments on Text.YAML.Construct).
	| Constructor !String
		//* An ADT constructor.
	| Record !String
		//* The name of a record (the record is not reflected in the YAML
		//* itself; see comments on Text.YAML.Construct).
	| Field !String
		//* A record field.
	| SequenceIndex !Int
		//* The index of a sequence (list or array).

//* Pushes the location to the locations stack of the `YAMLErrorWithLocations`.
pushErrorLocation :: !ErrorLocation !YAMLErrorWithLocations -> YAMLErrorWithLocations

/**
 * Load YAML into a native data structure. This involves parsing, composing a
 * node graph, and constructing the native graph (see the documentation on this
 * module).
 *
 * @param The schema to be used.
 * @param The YAML input.
 * @result An error or a value with a list of warnings. The error is a simple
 *   `YAMLError` in case the YAML could not be parsed or composed. The error is
 *   a `YAMLErrorWithLocations` in case the input is valid YAML but does not
 *   map to a Clean value of the requested type.
 */
loadYAML :: !YAMLSchema !String -> MaybeError (Either YAMLError YAMLErrorWithLocations) (a, [String]) | ConstructFromYAML a

/**
 * Like `loadYAML`, but accepts a `String` containing multiple YAML documents
 * (possibly zero). The documents are loaded independently into a native data
 * structure.
 */
loadMultipleYAML :: !YAMLSchema !String -> MaybeError (Either YAMLError YAMLErrorWithLocations) ([a], [String]) | ConstructFromYAML a
