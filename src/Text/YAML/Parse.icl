implementation module Text.YAML.Parse

/**
 * Copyright 2021 Camil Staps.
 *
 * This file is part of clean-yaml.
 *
 * Clean-yaml is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Clean-yaml is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * clean-yaml. If not, see <https://www.gnu.org/licenses/>.
 */

import StdEnv
import StdOverloadedList

import Control.Applicative
import Control.Monad
import Data.Either
import Data.Error
import Data.Func
import Data.Functor
import Data.List
import qualified Data.Map
import Data.Maybe
import Data.Tuple

import Text.YAML
import Text.YAML.Compose
import Text.YAML.ParseUtils

instance == YAMLEvent
where
	(==) (ScalarEvent t1 v1) (ScalarEvent t2 v2) = t1 == t2 && v1 == v2
	(==) (ScalarEvent _ _) _ = False
	(==) EnterSequence x = x=:EnterSequence
	(==) ExitSequence x = x=:ExitSequence
	(==) EnterMapping x = x=:EnterMapping
	(==) ExitMapping x = x=:ExitMapping
	(==) (PropertiesEvent x) (PropertiesEvent y) = x == y
	(==) (PropertiesEvent _) _ = False
	(==) (AliasEvent x) (AliasEvent y) = x == y
	(==) (AliasEvent _) _ = False
	(==) (EnterDocument x) (EnterDocument y) = x == y
	(==) (EnterDocument _) _ = False
	(==) (ExitDocument x) (ExitDocument y) = x == y
	(==) (ExitDocument _) _ = False
	(==) (WarnEvent x) (WarnEvent y) = x == y
	(==) (WarnEvent _) _ = False

:: EventParseState =
	{ events              :: ![!YAMLEvent]
	, tag_handles         :: !Map String String
	, seen_yaml_directive :: !Bool
	}

:: EventParser a :== StringParser EventParseState a

pTell :: !YAMLEvent -> EventParser ()
pTell ev = Parser \st -> ([((), {st & state.events=[|ev:st.state.events]})], [])

pTellScalar :: !YAMLScalarType !a -> EventParser () | toString a
pTellScalar type x = pTell (ScalarEvent type (toString x))

pSetTag :: !String !String -> EventParser ()
pSetTag handle prefix = Parser set
where
	set st=:{state={tag_handles}} =
		( [((), {st & state.tag_handles = 'Data.Map'.put handle prefix tag_handles})]
		, []
		)

pGetTag :: !String -> EventParser String
pGetTag handle = Parser \st -> case 'Data.Map'.get handle st.state.tag_handles of
	?None -> ([], [UnrecognizedTag handle])
	?Just prefix -> ([(prefix, st)], [])

unescapeHex :: !Char !Char -> Char
unescapeHex c1 c2 = toChar ((hexToInt c1 << 4) + hexToInt c2)

hexToInt :: !Char -> Int
hexToInt c
	| isDigit c
		= toInt (c - '0')
	| isLower c
		= toInt (c - 'a') + 10
		= toInt (c - 'A') + 10

:: Context :== (!BlockFlow, !InOutKey)
:: BlockFlow = Block | Flow
:: InOutKey = In | Out | Key

c_printable = pSatisfy (const True) // TODO

nb_json = pSatisfy (\c -> '\x20' <= c && c <= '\x7f' || c == '\x09') // TODO

c_sequence_entry = pToken '-'
c_mapping_key = pToken '?'
c_mapping_value = pToken ':'
c_collect_entry = pToken ','
c_sequence_start = pToken '['
c_sequence_end = pToken ']'
c_mapping_start = pToken '{'
c_mapping_end = pToken '}'
c_comment = pToken '#'
c_anchor = pToken '&'
c_alias = pToken '*'
c_tag = pToken '!'
c_literal = pToken '|'
c_folded = pToken '>'
c_single_quote = pToken '\''
c_double_quote = pToken '"'
c_directive = pToken '%'
c_reserved = pToken '@' <<|> pToken '`'

c_indicator
	=    c_sequence_entry <<|> c_mapping_key <<|> c_mapping_value
	<<|> c_collect_entry <<|> c_sequence_start <<|> c_sequence_end
	<<|> c_mapping_start <<|> c_mapping_end <<|> c_comment <<|> c_anchor
	<<|> c_alias <<|> c_tag <<|> c_literal <<|> c_folded <<|> c_single_quote
	<<|> c_double_quote <<|> c_directive <<|> c_reserved

c_flow_indicator
	=   c_collect_entry <<|> c_sequence_start <<|> c_sequence_end
	<<|> c_mapping_start <<|> c_mapping_end

b_line_feed = pToken '\n'
b_carriage_return = pToken '\r'
b_char = b_line_feed <<|> b_carriage_return

nb_char = pReject b_char >>| c_printable // NB: ignoring byte order mark

b_break
	=    (b_carriage_return >>| b_line_feed)
	<<|> b_carriage_return
	<<|> b_line_feed
b_as_line_feed = b_break $> ['\n']
b_non_content = b_break $> []

s_space = pToken ' '
s_tab = pToken '\t'
s_white = s_space <<|> s_tab
ns_char = pReject s_white >>| nb_char

ns_dec_digit = pSatisfy isDigit
ns_hex_digit = pSatisfy isHexDigit
ns_ascii_letter = pSatisfy isAlpha
ns_word_char = pSatisfy isAlphanum <<|> pToken '-'

ns_uri_char
	=    (pToken '%' >>| liftA2 unescapeHex ns_hex_digit ns_hex_digit)
	<<|> ns_word_char
	<<|> pSatisfy (\c -> IsMember c ns_uri_chars)
ns_uri_chars =: [#'#;/?:@&=+$,_.!~*\'()[]']

ns_tag_char = pReject (pToken '!' <<|> c_flow_indicator) >>| ns_uri_char

c_escape = pToken '\\'

ns_esc_null                = pToken '0' $> ['\x00']
ns_esc_bell                = pToken 'a' $> ['\x07']
ns_esc_backspace           = pToken 'b' $> ['\x08']
ns_esc_horizontal_tab      = (pToken 't' <<|> pToken '\t') $> ['\x09']
ns_esc_line_feed           = pToken 'n' $> ['\x0a']
ns_esc_vertical_tab        = pToken 'v' $> ['\x0b']
ns_esc_form_feed           = pToken 'f' $> ['\x0c']
ns_esc_carriage_return     = pToken 'r' $> ['\x0d']
ns_esc_escape              = pToken 'e' $> ['\x1b']
ns_esc_space               = pToken ' ' $> ['\x20']
ns_esc_double_quote        = pToken '"' $> ['\x22']
ns_esc_slash               = pToken '/' $> ['\x2f']
ns_esc_backslash           = pToken '\\' $> ['\x5c']
ns_esc_next_line           = pToken 'N' $> utf8encode 0x85
ns_esc_non_breaking_space  = pToken '_' $> utf8encode 0xa0
ns_esc_line_separator      = pToken 'L' $> utf8encode 0x2028
ns_esc_paragraph_separator = pToken 'P' $> utf8encode 0x2029
ns_esc_8_bit               = pToken 'x' >>| pure <$> liftA2 unescapeHex ns_hex_digit ns_hex_digit
ns_esc_16_bit              = pToken 'u' >>| unescapeUnicode4 <$>
	ns_hex_digit <*> ns_hex_digit <*> ns_hex_digit <*> ns_hex_digit
ns_esc_32_bit              = pToken 'U' >>| unescapeUnicode8 <$>
	ns_hex_digit <*> ns_hex_digit <*> ns_hex_digit <*> ns_hex_digit <*>
	ns_hex_digit <*> ns_hex_digit <*> ns_hex_digit <*> ns_hex_digit

unescapeUnicode4 x1 x2 x3 x4 = utf8encode
	((hexToInt x1 << 12) + (hexToInt x2 << 8) + (hexToInt x3 << 4) + hexToInt x4)
unescapeUnicode8 x1 x2 x3 x4 x5 x6 x7 x8 = utf8encode
	((hexToInt x1 << 28) + (hexToInt x2 << 24) + (hexToInt x3 << 20) + (hexToInt x4 << 16) +
	(hexToInt x5 << 12) + (hexToInt x6 << 8) + (hexToInt x7 << 4) + hexToInt x8)

utf8encode c
	| c < 0x7f =
		[ toChar c
		]
	| c < 0x7ff =
		[ toChar (0xc0 + ((c >> 6) bitand 0x1f))
		, toChar (0x80 + (c bitand 0x3f))
		]
	| c < 0xffff =
		[ toChar (0xe0 + ((c >> 12) bitand 0x0f))
		, toChar (0x80 + ((c >> 6) bitand 0x3f))
		, toChar (0x80 + (c bitand 0x3f))
		]
	| otherwise =
		[ toChar (0xf0 + ((c >> 18) bitand 0x07))
		, toChar (0x80 + ((c >> 12) bitand 0x3f))
		, toChar (0x80 + ((c >> 6) bitand 0x3f))
		, toChar (0x80 + (c bitand 0x3f))
		]

c_ns_esc_char = c_escape >>|
	((   ns_esc_null <<|> ns_esc_bell <<|> ns_esc_backspace
	<<|> ns_esc_horizontal_tab <<|> ns_esc_line_feed <<|> ns_esc_vertical_tab
	<<|> ns_esc_form_feed <<|> ns_esc_carriage_return <<|> ns_esc_escape
	<<|> ns_esc_space <<|> ns_esc_double_quote <<|> ns_esc_slash
	<<|> ns_esc_backslash <<|> ns_esc_next_line <<|> ns_esc_non_breaking_space
	<<|> ns_esc_line_separator <<|> ns_esc_paragraph_separator
	<<|> ns_esc_8_bit <<|> ns_esc_16_bit <<|> ns_esc_32_bit
	) @! IllFormed "invalid escape sequence")

s_indent n = sequence (repeatn n s_space)

s_indent_lt n =
	pMany s_space >>= \spaces
		| length spaces >= n
			-> pError (IllFormed "too much indentation")
			-> pure spaces

s_indent_le n = s_indent_lt (n+1)

s_separate_in_line = pSome s_white <<|> (pStartOfLine $> [])

s_line_prefix n (Block,_) = s_block_line_prefix n
s_line_prefix n (Flow,_) = s_flow_line_prefix n
s_block_line_prefix n = s_indent n
s_flow_line_prefix n = s_indent n <* pFastOptional s_separate_in_line

l_empty n c = (s_line_prefix n c <<|> s_indent_lt n) >>| b_as_line_feed

b_l_trimmed n c = b_non_content >>| (flatten <$> pSome (l_empty n c))
b_as_space = b_break $> [' ']
b_l_folded n c = b_l_trimmed n c <<|> b_as_space

s_flow_folded n =
	pFastOptional s_separate_in_line >>|
	b_l_folded n (Flow,In) <* s_flow_line_prefix n

c_nb_comment_text = pToken '#' >>| pMany nb_char
b_comment = b_non_content <<|> pEof
s_b_comment = pFastOptional (s_separate_in_line >>| pFastOptional c_nb_comment_text) >>| b_comment

l_comment = s_separate_in_line >>| pFastOptional c_nb_comment_text >>| b_comment
s_l_comments = (s_b_comment <<|> (pStartOfLine $> [])) >>| pMany (pNonEmpty l_comment) $> ()

s_separate n (_,Key) = s_separate_in_line
s_separate n _ = s_separate_lines n

s_separate_lines n
	=    (s_l_comments >>| s_flow_line_prefix n)
	<<|> s_separate_in_line

l_directive =
	pToken '%' >>|
	(ns_yaml_directive <?|> ns_tag_directive <?|> ns_reserved_directive) <*
	s_l_comments

ns_reserved_directive =
	(toString <$> ns_directive_name) >>= \name
		| name == "YAML" || name == "TAG" -> pFail
		| otherwise ->
			(map toString <$> pMany (s_separate_in_line >>| ns_directive_parameter)) >>|
			pTell (WarnEvent ("Reserved directive: "+++name))
ns_directive_name = pSome ns_char
ns_directive_parameter = pSome ns_char

ns_yaml_directive =
	pToken 'Y' >>| pToken 'A' >>| pToken 'M' >>| pToken 'L' >>|
	Parser checkIfDirectiveHasBeenSeen >>|
	s_separate_in_line >>|
	ns_yaml_version >>= \(major,minor)
		| major <> 1
			-> pError (UnsupportedSyntax "only YAML 1.2+ is supported")
		| minor < 2
			-> pError (UnsupportedSyntax "only YAML 1.2+ is supported")
		| minor > 2
			-> pTell (WarnEvent ("unexpected version 1." +++ toString minor +++ ", parsing as 1.2"))
			-> pure ()
where
	checkIfDirectiveHasBeenSeen st
		| st.state.seen_yaml_directive
			= ([], [IllFormed "repeated YAML directive"])
			= ([((), {st & state.seen_yaml_directive=True})], [])
ns_yaml_version = liftA2 tuple
	(toInt <$> toString <$> pSome ns_dec_digit)
	(pToken '.' >>| (toInt <$> toString <$> pSome ns_dec_digit))

ns_tag_directive =
	pToken 'T' >>| pToken 'A' >>| pToken 'G' >>|
	s_separate_in_line >>| c_tag_handle >>= \handle ->
	s_separate_in_line >>| ns_tag_prefix >>= \prefix ->
	pSetTag handle (toString prefix)

c_tag_handle = c_named_tag_handle <?|> c_secondary_tag_handle <?|> c_primary_tag_handle

c_primary_tag_handle = pToken '!' $> "!"
c_secondary_tag_handle = pToken '!' >>| pToken '!' $> "!!"
c_named_tag_handle = pToken '!' >>| (toString <$> pSome ns_word_char) <* pToken '!'

ns_tag_prefix = c_ns_local_tag_prefix <<|> ns_global_tag_prefix
c_ns_local_tag_prefix = pToken '!' <:> pMany ns_uri_char
ns_global_tag_prefix = ns_tag_char <:> pMany ns_uri_char

c_ns_properties n c
	=    (c_ns_tag_property >>= \t -> pOptional (s_separate n c >>| c_ns_anchor_property) >>= \a -> pTell (PropertiesEvent {tag=t, anchor=a}))
	<<|> (c_ns_anchor_property >>= \a -> pOptional (s_separate n c >>| c_ns_tag_property) >>= \t -> pTell (PropertiesEvent {tag=fromMaybe "" t, anchor= ?Just a}))

c_ns_tag_property
	=    c_verbatim_tag
	<?|> c_ns_shorthand_tag
	<?|> c_non_specific_tag
c_verbatim_tag = toString <$> (pToken '!' >>| pToken '<' >>| pSome ns_uri_char <* pToken '>')
c_ns_shorthand_tag = liftA2 (+++) (c_tag_handle >>= pGetTag) (toString <$> pSome ns_tag_char)
c_non_specific_tag = pToken '!' $> "!"

c_ns_anchor_property = pToken '&' >>| ns_anchor_name
ns_anchor_char = pReject c_flow_indicator >>| ns_char
ns_anchor_name = toString <$> pSome ns_anchor_char

c_ns_alias_node = pToken '*' >>| ns_anchor_name >>= pTell o AliasEvent

e_scalar = pTellScalar Plain ""
e_node = e_scalar

nb_double_char = c_ns_esc_char <<|> (pReject (pToken '\\' <<|> pToken '"') >>| pure <$> nb_json)
ns_double_char = pReject s_white >>| nb_double_char

c_double_quoted n c =
	pToken '"' >>|
	nb_double_text n c >>= pTellScalar DoubleQuoted >>|
	pToken '"' $> ()

nb_double_text n (Flow, Out) = nb_double_multi_line n
nb_double_text n (Flow, In)  = nb_double_multi_line n
nb_double_text n (_, Key)    = nb_double_one_line

nb_double_one_line = flatten <$> pMany nb_double_char

s_double_escaped n =
	pMany s_white <* pToken '\\' <* b_non_content <*
	pMany (l_empty n (Flow,In)) <* s_flow_line_prefix n
s_double_break n = s_double_escaped n <<|> s_flow_folded n

nb_ns_double_in_line = flatten <$> pMany (liftA2 (\w c -> w ++ c) (pMany s_white) ns_double_char)
s_double_next_line n = liftA2 (++)
	(s_double_break n)
	(fromMaybe [] <$> pFastOptional (liftA3 (\c x xs -> c ++ x ++ xs)
		ns_double_char
		nb_ns_double_in_line
		(s_double_next_line n <<|> pMany s_white)
	))
nb_double_multi_line n = liftA2 (++)
	nb_ns_double_in_line
	(s_double_next_line n <<|> pMany s_white)

c_quoted_quote = pToken '\'' >>| pToken '\''
nb_single_char = c_quoted_quote <<|> (pReject (pToken '\'') >>| nb_json)
ns_single_char = pReject s_white >>| nb_single_char

c_single_quoted n c =
	pToken '\'' >>|
	nb_single_text n c >>= pTellScalar SingleQuoted >>|
	pToken '\'' $> ()

nb_single_text n (Flow, Out) = nb_single_multi_line n
nb_single_text n (Flow, In)  = nb_single_multi_line n
nb_single_text _ (_, Key)    = nb_single_one_line

nb_single_one_line = pMany nb_single_char

nb_ns_single_in_line = flatten <$> pMany (liftA2 (++) (pMany s_white) (pure <$> ns_single_char))
s_single_next_line n = liftA2 (++)
	(s_flow_folded n)
	(fromMaybe [] <$> pFastOptional (liftA3 (\c x xs -> [c:x ++ xs])
		ns_single_char
		nb_ns_single_in_line
		(s_single_next_line n <<|> pMany s_white)
	))
nb_single_multi_line n = liftA2 (++)
	nb_ns_single_in_line
	(s_single_next_line n <<|> pMany s_white)

ns_plain_first c
	=    (pReject c_indicator >>| ns_char)
	<<|> (pSatisfy (\c -> isMember c ['?:-']) <* pCheck (ns_plain_safe c))

ns_plain_safe (Flow,Out)  = ns_plain_safe_out
ns_plain_safe (Flow,In)   = ns_plain_safe_in
ns_plain_safe (Block,Key) = ns_plain_safe_out
ns_plain_safe (Flow,Key)  = ns_plain_safe_in

ns_plain_safe_out = ns_char
ns_plain_safe_in = pReject c_flow_indicator >>| ns_char

ns_plain_char c
	=    (pReject (pToken ':' <<|> pToken '#') >>| ns_plain_safe c)
	<<|> (pRewind >>| ns_char >>| pToken '#')
	<<|> (pToken ':' <* pCheck (ns_plain_safe c))

ns_plain n c=:(Flow,Out)  = ns_plain_multi_line n c >>= pTellScalar Plain
ns_plain n c=:(Flow,In)   = ns_plain_multi_line n c >>= pTellScalar Plain
ns_plain n c=:(Block,Key) = ns_plain_one_line c >>= pTellScalar Plain
ns_plain n c=:(Flow,Key)  = ns_plain_one_line c >>= pTellScalar Plain

nb_ns_plain_in_line c = flatten <$> pMany (liftA2 (\w c -> w ++ [c]) (pMany s_white) (ns_plain_char c))
ns_plain_one_line c = ns_plain_first c <:> nb_ns_plain_in_line c

s_ns_plain_next_line n c = liftA2 (++) (s_flow_folded n) (ns_plain_char c <:> nb_ns_plain_in_line c)
ns_plain_multi_line n c = flatten <$> ns_plain_one_line c <:> pMany (s_ns_plain_next_line n c)

in_flow (Flow,Out)  = (Flow, In)
in_flow (Flow,In)   = (Flow, In)
in_flow (Block,Key) = (Flow, Key)
in_flow (Flow,Key)  = (Flow, Key)

c_flow_sequence n c =
	pToken '[' >>| pTell EnterSequence >>|
	pFastOptional (s_separate n c) >>|
	pFastOptional (ns_s_flow_seq_entries n (in_flow c)) >>|
	pToken ']' >>| pTell ExitSequence
ns_s_flow_seq_entries n c =
	ns_flow_seq_entry n c >>| pFastOptional (s_separate n c) >>|
	pFastOptional (
		pToken ',' >>| pFastOptional (s_separate n c) >>|
		(fromMaybe [] o join <$> pFastOptional (ns_s_flow_seq_entries n c))
	)
ns_flow_seq_entry n c
	=    (pTell EnterMapping >>| ns_flow_pair n c >>| pTell ExitMapping)
	<<|> ns_flow_node n c

c_flow_mapping n c =
	pToken '{' >>| pTell EnterMapping >>|
	pFastOptional (s_separate n c) >>|
	pFastOptional (ns_s_flow_map_entries n (in_flow c)) >>|
	pToken '}' >>| pTell ExitMapping
ns_s_flow_map_entries n c =
	ns_flow_map_entry n c >>| pFastOptional (s_separate n c) >>|
	pFastOptional (
		pToken ',' >>| pFastOptional (s_separate n c) >>|
		(fromMaybe [] o join <$> pFastOptional (ns_s_flow_map_entries n c))
	)
ns_flow_map_entry n c
	=    (pToken '?' >>| s_separate n c >>| ns_flow_map_explicit_entry n c)
	<<|> ns_flow_map_implicit_entry n c
ns_flow_map_explicit_entry n c
	=    ns_flow_map_implicit_entry n c
	<<|> (e_node >>| e_node)
ns_flow_map_implicit_entry n c
	=    ns_flow_map_yaml_key_entry n c
	<?|> c_ns_flow_map_empty_key_entry n c
	<<|> c_ns_flow_map_json_key_entry n c
ns_flow_map_yaml_key_entry n c =
	ns_flow_yaml_node n c >>|
	(    (pFastOptional (s_separate n c) >>| c_ns_flow_map_separate_value n c)
	<<|> e_node
	)
c_ns_flow_map_empty_key_entry n c = e_node >>| c_ns_flow_map_separate_value n c
c_ns_flow_map_separate_value n c =
	pToken ':' >>| pReject (ns_plain_safe c) >>|
	((s_separate n c >>| ns_flow_node n c) <<|> e_node)
c_ns_flow_map_json_key_entry n c =
	c_flow_json_node n c >>|
	(    (pFastOptional (s_separate n c) >>| c_ns_flow_map_adjacent_value n c)
	<<|> e_node
	)
c_ns_flow_map_adjacent_value n c =
	pToken ':' >>|
	(    pFastOptional (s_separate n c) >>| ns_flow_node n c
	<<|> e_node
	)

ns_flow_pair n c
	=    (pToken '?' >>| s_separate n c >>| ns_flow_map_explicit_entry n c)
	<<|> ns_flow_pair_entry n c
ns_flow_pair_entry n c
	=    ns_flow_pair_yaml_key_entry n c
	<<|> c_ns_flow_map_empty_key_entry n c
	<<|> c_ns_flow_pair_json_key_entry n c
ns_flow_pair_yaml_key_entry n c =
	ns_s_implicit_yaml_key (Flow,Key) >>|
	c_ns_flow_map_separate_value n c
c_ns_flow_pair_json_key_entry n c =
	c_s_implicit_json_key (Flow,Key) >>|
	c_ns_flow_map_adjacent_value n c
ns_s_implicit_yaml_key c = ns_flow_yaml_node -1 c <* pFastOptional s_separate_in_line // TODO max 1024 chars
c_s_implicit_json_key c = c_flow_json_node -1 c <* pFastOptional s_separate_in_line // TODO max 1024 chars

ns_flow_yaml_content n c = ns_plain n c
c_flow_json_content n c
	=    c_flow_sequence n c
	<<|> c_flow_mapping n c
	<<|> c_single_quoted n c
	<<|> c_double_quoted n c
ns_flow_content n c
	=    ns_flow_yaml_content n c
	<<|> c_flow_json_content n c

ns_flow_yaml_node n c
	=    c_ns_alias_node
	<<|> ns_flow_yaml_content n c
	<<|> (c_ns_properties n c >>| ((s_separate n c >>| ns_flow_yaml_content n c) <<|> e_scalar))
c_flow_json_node n c =
	pFastOptional (c_ns_properties n c <* s_separate n c) >>|
	c_flow_json_content n c
ns_flow_node n c
	=    c_ns_alias_node
	<<|> ns_flow_content n c
	<<|> (c_ns_properties n c >>| ((s_separate n c >>| ns_flow_content n c) <<|> e_scalar))

c_b_block_header n
	=    (liftA2 tuple (c_indentation_indicator n) c_chomping_indicator <* s_b_comment)
	<<|> (liftA2 (flip tuple) c_chomping_indicator (c_indentation_indicator n) <* s_b_comment)

c_indentation_indicator n
	=    (nonZeroToInt =<< ns_dec_digit)
	<<|> autoDetectIndentation True n
where
	nonZeroToInt c
		| '1' <= c && c <= '9'
			= pure (digitToInt c)
			= pError (IllFormed "explicit indentation level must be between 1 and 9")

// Normally we check that:
// 1. we have an indentation (i.e. we do not reach the end of the stream)
// 2. the indentation found is larger than the start indentation
// 3. the indentation found is larger than any lines containing only spaces
// However, scalars can be empty, so conditions (1) and (2) must be lifted. We
// do this by setting acceptNoContent to True. In this case we return `n` + the
// number of spaces on the longest line (from which n is subtracted again in
// `detect`). This is conform the spec, which states that indentation is
// detected based on the longest empty line instead if there is no non-empty
// line.
autoDetectIndentation acceptNoContent n = Parser detect
where
	detect st=:{s,idx}
		# i = if at_start_of_line idx (skip_to_next_line idx)
		= case find_indentation (n+1) 0 i of
			?None ->
				([], [])
			?Just m ->
				([(m-n, st)], [])
	where
		at_start_of_line = idx == 0 || s.[idx-1] == '\n' || s.[idx-1] == '\r'

		find_indentation min_empty m i
			| i >= size s
				| acceptNoContent
					= ?Just (n+min_empty) // scalar to end of stream
					= ?None
			| s.[i] == ' '
				= find_indentation min_empty (m+1) (i+1)
			| s.[i] == '\r'
				= find_indentation (max min_empty m) 0 (skip_to_next_line i)
			| s.[i] == '\n'
				= find_indentation (max min_empty m) 0 (i+1)
			| m >= min_empty
				= ?Just m
			| acceptNoContent && m == 0
				= ?Just (n+min_empty) // empty scalar
				= ?None

		skip_to_next_line i
			| i >= size s
				= i
			| s.[i] == '\r'
				= if (i+1 < size s-1 && s.[i+1] == '\n') (i+2) (i+1)
			| s.[i] == '\n'
				= i+1
				= skip_to_next_line (i+1)

:: ChompingMethod = Strip | Clip | Keep

c_chomping_indicator
	=    (pToken '-' $> Strip)
	<<|> (pToken '+' $> Keep)
	<<|> pure Clip

b_chomped_last Strip = b_non_content <<|> pEof
b_chomped_last Clip  = b_as_line_feed <<|> pEof
b_chomped_last Keep  = b_as_line_feed <<|> pEof

l_chomped_empty n Strip = l_strip_empty n
l_chomped_empty n Clip  = l_strip_empty n
l_chomped_empty n Keep  = l_keep_empty n

l_strip_empty n = pMany (s_indent_le n >>| b_non_content) <* pFastOptional (l_trail_comments n)
l_keep_empty n = pMany (l_empty n (Block,In)) <* pFastOptional (l_trail_comments n)

l_trail_comments n = s_indent_lt n >>| c_nb_comment_text >>| b_comment >>| pMany (pNonEmpty l_comment)

c_l__literal n =
	pToken '|' >>| c_b_block_header n >>= \(m,t) ->
	l_literal_content (n+m) t >>= pTellScalar Literal

l_nb_literal_text n = liftA2 (++)
	(pMany (l_empty n (Block,In) $> '\n'))
	(s_indent n >>| pSome nb_char)
b_nb_literal_next n = liftA2 (++)
	b_as_line_feed
	(l_nb_literal_text n)
l_literal_content n t = liftA2 (++)
	(fromMaybe [] <$> pFastOptional (liftA3 (\x y z -> x ++ y ++ z)
		(l_nb_literal_text n)
		(flatten <$> pMany (b_nb_literal_next n))
		(b_chomped_last t)
	))
	(flatten <$> l_chomped_empty n t)

c_l__folded n =
	pToken '>' >>| c_b_block_header n >>= \(m,t) ->
	l_folded_content (n+m) t >>= pTellScalar Folded

s_nb_folded_text n = s_indent n >>| ns_char <:> pMany nb_char
l_nb_folded_lines n =
	s_nb_folded_text n <:>
	pMany (liftA2 (++) (b_l_folded n (Block,In)) (s_nb_folded_text n))

s_nb_spaced_text n = s_indent n >>| s_white <:> pMany nb_char
b_l_spaced n = flatten <$> b_as_line_feed <:> pMany (l_empty n (Block,In))
l_nb_spaced_lines n =
	s_nb_spaced_text n <:>
	pMany (liftA2 (++) (b_l_spaced n) (s_nb_spaced_text n))

l_nb_same_lines n = flatten <$> liftA2 (++)
	(pMany (l_empty n (Block,In)))
	(l_nb_folded_lines n <<|> l_nb_spaced_lines n)
l_nb_diff_lines n = flatten <$>
	l_nb_same_lines n <:>
	pMany (liftA2 (++) b_as_line_feed (l_nb_same_lines n))

l_folded_content n t =
	fromMaybe [] <$> pFastOptional (liftA2 (++) (l_nb_diff_lines n) (b_chomped_last t)) <*
	l_chomped_empty n t

l__block_sequence n =
	pTell EnterSequence >>|
	(((+) n) <$> autoDetectIndentation False n) >>= \n ->
	pSome (s_indent n >>| c_l_block_seq_entry n) >>|
	pTell ExitSequence
c_l_block_seq_entry n =
	pToken '-' >>| pReject ns_char >>|
	s_l__block_indented n (Block,In)

s_l__block_indented n c
	=    ((length <$> pSome s_space) >>= \m -> ns_l_compact_sequence (n+1+m) <<|> ns_l_compact_mapping (n+1+m))
	<<|> s_l__block_node n c
	<<|> (e_node <* s_l_comments)
ns_l_compact_sequence n =
	pTell EnterSequence >>|
	pSepBy1 (c_l_block_seq_entry n) (s_indent n) >>|
	pTell ExitSequence

l__block_mapping n =
	pTell EnterMapping >>|
	(((+) n) <$> autoDetectIndentation False n) >>= \n ->
	pSome (s_indent n >>| ns_l_block_map_entry n) >>|
	pTell ExitMapping

ns_l_block_map_entry n
	=    c_l_block_map_explicit_entry n
	<<|> ns_l_block_map_implicit_entry n
c_l_block_map_explicit_entry n =
	c_l_block_map_explicit_key n >>|
	(l_block_map_explicit_value n <<|> e_node)
c_l_block_map_explicit_key n = pToken '?' >>| s_l__block_indented n (Block,Out)
l_block_map_explicit_value n = s_indent n >>| pToken ':' >>| s_l__block_indented n (Block,Out)

ns_l_block_map_implicit_entry n =
	(ns_s_block_map_implicit_key <<|> e_node) >>|
	c_l_block_map_implicit_value n
ns_s_block_map_implicit_key
	=    c_s_implicit_json_key (Block,Key)
	<<|> ns_s_implicit_yaml_key (Block,Key)

c_l_block_map_implicit_value n = pToken ':' >>| (s_l__block_node n (Block,Out) <<|> (e_node <* s_l_comments))

ns_l_compact_mapping n =
	pTell EnterMapping >>|
	pSepBy1 (ns_l_block_map_entry n) (s_indent n) >>|
	pTell ExitMapping

s_l__block_node n c
	=    s_l__block_in_block n c
	<<|> s_l__flow_in_block n
s_l__flow_in_block n = s_separate (n+1) (Flow,Out) >>| ns_flow_node (n+1) (Flow,Out) <* s_l_comments

s_l__block_in_block n c
	=    s_l__block_scalar n c
	<<|> s_l__block_collection n c
s_l__block_scalar n c =
	s_separate (n+1) c >>| pFastOptional (c_ns_properties (n+1) c >>| s_separate (n+1) c) >>|
	(c_l__literal n <<|> c_l__folded n)

s_l__block_collection n c =
	pOptional (s_separate (n+1) c >>| c_ns_properties (n+1) c) >>| s_l_comments >>|
	(l__block_sequence (seq_spaces n c) <<|> l__block_mapping n)
seq_spaces n (Block,Out) = n-1
seq_spaces n (Block,In) = n

l_document_prefix =
	// TODO c_byte_order_mark
	pMany (pNonEmpty l_comment)

c_directives_end = pToken '-' >>| pToken '-' >>| pToken '-' >>| pTell (EnterDocument True)
c_document_end =
	pToken '.' >>| pToken '.' >>| pToken '.' >>|
	isInDocument >>= \in_document
		| in_document ->
			pTell (ExitDocument True)
		| otherwise ->
			pure ()
where
	isInDocument = Parser \st ->
		([(not (IsEmpty st.state.events || (Hd st.state.events)=:(ExitDocument _)), st)], [])
l_document_suffix = c_document_end >>| s_l_comments

c_forbidden =
	pStartOfLine >>|
	(c_directives_end <<|> c_document_end) >>|
	(b_char <<|> s_white <<|> (pEof $> '_'))

// NB: this has been split up to only emit `EnterDocument False` when needed
l_bare_document =
	pTell (EnterDocument False) >>|
	l_bare_document2
l_bare_document2 = s_l__block_node -1 (Block,In) `forbidding` c_forbidden

l_explicit_document =
	c_directives_end >>|
	(l_bare_document2 <<|> (e_node >>| s_l_comments))

l_directive_document =
	pSome l_directive >>|
	l_explicit_document

l_any_document =
	resetForNewDocument >>|
	(l_directive_document <?|> l_explicit_document <?|> l_bare_document)
l_yaml_stream =
	pMany (pNonEmpty l_document_prefix) >>|
	pOptional l_any_document >>|
	pMany
		(
			(pSome l_document_suffix >>|
				pMany (pNonEmpty l_document_prefix) >>|
				pOptional l_any_document
			)
		<<|>
			(pTell (ExitDocument False) >>|
				pMany (pNonEmpty l_document_prefix) >>|
				resetForNewDocument >>|
				(?Just <$> l_explicit_document)
			)
		) >>|
	(c_document_end <<|> mbExitDocument)
where
	// To emit the final `ExitDocument`:
	mbExitDocument = Parser \st=:{state={events}}
		| IsEmpty events || (Hd events)=:(ExitDocument _)
			-> ([((), st)], [])
			-> ([((), {st & state.events=[|ExitDocument False:events]})], [])

resetForNewDocument :: EventParser ()
resetForNewDocument = Parser \st -> ([((), update st)], [])
where
	update st =
		{ st
		& state.tag_handles = default_tag_handles
		, state.seen_yaml_directive = False
		}
default_tag_handles =: 'Data.Map'.fromList
	[ ("!", "!")
	, ("!!", "tag:yaml.org,2002:")
	]

parseYAMLStream :: !String -> MaybeError YAMLError [YAMLEvent]
parseYAMLStream s = (\(_,st) -> setNonSpecificTags $ ReverseM st.events) <$> parse l_yaml_stream initState s
where
	initState =
		{ events              = [!]
		, tag_handles         = 'Data.Map'.newMap
		, seen_yaml_directive = False
		}

	// Scalars get the non-specific tag ! whereas other nodes get ?. We cannot
	// do this in the parser without lookahead, so we do it as a final pass.
	setNonSpecificTags [PropertiesEvent p=:{tag=""}, s=:(ScalarEvent Plain _) : rest] =
		[PropertiesEvent {p & tag="?"}, s : setNonSpecificTags rest]
	setNonSpecificTags [PropertiesEvent p=:{tag=""}, s=:(ScalarEvent _ _) : rest] =
		[PropertiesEvent {p & tag="!"}, s : setNonSpecificTags rest]
	setNonSpecificTags [PropertiesEvent p=:{tag=""} : rest] =
		[PropertiesEvent {p & tag="?"} : setNonSpecificTags rest]
	setNonSpecificTags [e:rest] =
		[e:setNonSpecificTags rest]
	setNonSpecificTags [] =
		[]
