definition module Text.YAML.Represent

/**
 * This module provides the first step of dumping data as YAML: representing a
 * native data structure as a node graph.
 *
 * For documentation on the representation, see Text.YAML.Construct.
 *
 * Copyright 2021-2023 Camil Staps.
 * Copyright 2021 Gijs Alberts.
 *
 * This file is part of clean-yaml.
 *
 * Clean-yaml is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Clean-yaml is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * clean-yaml. If not, see <https://www.gnu.org/licenses/>.
 *
 * @property-bootstrap
 *   import Control.Monad
 *   import Data.Error
 *   import Data.Error.GenPrint
 *   import Data.Functor
 *   import Data.GenEq
 *   import Data.List
 *   import Data.Real
 *   import StdEnv
 *   import StdMaybe
 *
 *   import Text.YAML
 *   import Text.YAML.Compose
 *   import Text.YAML.Construct
 *   import Text.YAML.Serialize
 *
 *   instance == ErrorLocation derive gEq
 *   instance == YAMLError derive gEq
 *   instance == YAMLErrorWithLocations derive gEq
 *
 *   derive genShow ErrorLocation, MaybeError, YAMLError, YAMLErrorWithLocations
 *   derive gPrint ErrorLocation, YAMLError, YAMLErrorWithLocations
 *
 *   // Some random type that can cover lots of cases
 *   :: R elem =
 *     { int    :: Int
 *     , adts   :: [elem]
 *     , mb_int :: ?elem
 *     }
 *   :: ADT = NoArg | OneArg Char | TwoArgs () Char
 *
 *   derive bimap [!]
 *   derive class Gast R, ADT
 *   derive gRepresentAsYAML R, ADT
 *   instance ConstructFromYAML (R e) | ConstructFromYAML e derive gConstructFromYAML
 *   instance ConstructFromYAML ADT derive gConstructFromYAML
 *   instance == (R e) | == e derive gEq
 *   instance == ADT derive gEq
 *
 *   (real_equals) parsed generated
 *     | isNaN generated
 *       = prop (isNaN parsed)
 *     | generated >= LargestReal
 *       = check equal parsed Infinity
 *     | generated <= ~LargestReal
 *       = check equal parsed (~Infinity)
 *       = check equal parsed generated
 *   where
 *     equal = approximatelyEqual 0.000001
 *
 * @property-test-with t = R Char
 * @property-test-with t = R ADT
 * @property-test-with t = R (String, ())
 */

from _SystemStrictLists import class UList, class UTSList
from _SystemStrictMaybes import class UMaybe
import StdGeneric

from Data.Error import :: MaybeError

from Text.YAML import :: YAMLError
from Text.YAML.Compose import :: YAMLNode
from Text.YAML.Schemas import :: YAMLSchema

/**
 * @property round-trip through node graph: A.x :: t:
 *     isOk represented ==> constructed =.= Ok x
 *   where
 *     represented = representAsYAML coreSchema x
 *     constructed = constructFromYAML coreSchema (dynamic ()) (fromOk represented)
 *
 * @property round-trip through node graph (real): A.x :: Real:
 *     isOk represented ==>
 *       isOk constructed /\
 *       fromOk constructed real_equals x
 *   where
 *     represented = representAsYAML coreSchema x
 *     constructed = constructFromYAML coreSchema (dynamic ()) (fromOk represented)
 *
 * @property round-trip through event tree: A.x :: t:
 *     isOk serialized ==> constructed =.= Ok x
 *   where
 *     serialized = serializeYAMLDocument <$> representAsYAML coreSchema x
 *     constructed = mapError withoutErrorLocations (composeYAMLGraphs (fromOk serialized)) >>= constructFromYAML coreSchema (dynamic ()) o hd
 */
representAsYAML :: !YAMLSchema !a -> MaybeError YAMLError YAMLNode | gRepresentAsYAML{|*|} a

/**
 * Custom instances should yield a list with one element. The result is a list
 * to implement the `PAIR` instance.
 *
 * @param The schema to be used (needed to generate non-specific tags if
 *   possible).
 * @param Whether we are generating a record field.
 * @param The value.
 * @result A list with one element (the represented node graph), or an error.
 */
generic gRepresentAsYAML a :: !YAMLSchema !Bool !a -> MaybeError YAMLError [YAMLNode]

derive gRepresentAsYAML UNIT, EITHER, PAIR, OBJECT, CONS of {gcd_name,gcd_arity}, RECORD of {grd_fields}, FIELD
derive gRepresentAsYAML String, Int, Real, Char
derive gRepresentAsYAML [], [!], [ !], [!!], [#], [#!], {}, {!}, ?, ?^, ?#
derive gRepresentAsYAML (), (,), (,,), (,,,), (,,,,), (,,,,,), (,,,,,,), (,,,,,,,)
