definition module Text.YAML.Compose

/**
 * This module provides the second step of reading YAML files: composing a node
 * graph from an event tree.
 *
 * Copyright 2021 Camil Staps.
 *
 * This file is part of clean-yaml.
 *
 * Clean-yaml is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Clean-yaml is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * clean-yaml. If not, see <https://www.gnu.org/licenses/>.
 */

from StdOverloaded import class ==

from Data.Error import :: MaybeError

from Text.YAML import :: YAMLError
from Text.YAML.Parse import :: YAMLEvent

:: YAMLNode =
	{ properties :: !YAMLNodeProperties
	, content    :: !YAMLNodeContent
	}

:: YAMLNodeContent
	= YAMLScalar !YAMLScalarType !String
	| YAMLSequence ![YAMLNode]
	| YAMLMapping ![YAMLKeyValuePair]
	| YAMLAlias !String

:: YAMLScalarType
	= Plain
	| SingleQuoted | DoubleQuoted
	| Literal | Folded

:: YAMLKeyValuePair =
	{ key   :: !YAMLNode
	, value :: !YAMLNode
	}

:: YAMLNodeProperties =
	{ tag    :: !String
	, anchor :: !?String
	}

instance == YAMLNode, YAMLNodeContent, YAMLScalarType, YAMLKeyValuePair, YAMLNodeProperties

composeYAMLGraphs :: ![YAMLEvent] -> MaybeError YAMLError [YAMLNode]
