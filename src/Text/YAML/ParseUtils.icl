implementation module Text.YAML.ParseUtils

/**
 * Copyright 2021 Camil Staps.
 *
 * This file is part of clean-yaml.
 *
 * Clean-yaml is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Clean-yaml is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * clean-yaml. If not, see <https://www.gnu.org/licenses/>.
 */

import StdEnv
import StdMaybe

import Control.Applicative
import Control.Monad
import Data.Error
import Data.Func
import Data.Functor
import Data.Tuple
from Data.Map import :: Map
import qualified Data.Map

import Text.YAML
import Text.YAML.Parse

parse :: !(Parser arr t st a) !st !(arr t) -> MaybeError YAMLError (a, st) | Array arr t
parse p state s = case runParser p (initState s) of
	(rs,[]) -> case [r \\ r=:(_,st) <- rs | st.idx == size s] of
		[(x,st):_] -> Ok (x, st.state)
		_ -> Error (IllFormed ("failed to parse at index "+++toString (maxIndex rs)))
	(_,es) -> Error (hd es)
where
	initState s =
		{ s         = s
		, idx       = 0
		, state     = state
		, forbidden = ?None
		}

	maxIndex results = maxList [st.idx \\ (_,st) <- results]

runParser :: !(Parser arr t st a) (ParseState arr t st) -> ([(a, ParseState arr t st)], [YAMLError])
runParser (Parser p) st = p st

instance Functor (Parser arr t st)
where
	fmap f (Parser p) = Parser (appFst (map (appFst f)) o p)

instance pure (Parser arr t st) where pure x = Parser \st -> ([(x, st)], [])

instance <*> (Parser arr t st) where (<*>) l r = ap l r
instance <* (Parser arr t st)

instance Monad (Parser arr t st)
where
	bind (Parser l) r = Parser \st
		# (rs1,es1) = l st
		| isEmpty rs1 = ([], es1)
		# (rs`,es`) = unzip
			[ (rs2, es1 ++ es2)
			\\ (r1,st1) <- rs1
			, let (rs2,es2) = runParser (r r1) st1
			]
		= (flatten rs`, flatten es`)

pFail :: Parser arr t st a
pFail = Parser \_ -> ([], [])

pError :: !YAMLError -> Parser arr t st a
pError e = Parser \_ -> ([], [e])

(@!) infixr 4 :: (Parser arr t st a) !YAMLError -> Parser arr t st a
(@!) p e = p <<|> pError e

pSatisfy :: !(t -> Bool) -> Parser arr t st t | Array arr t
pSatisfy pred = Parser \st
	| st.idx >= size st.s
		-> ([], [])
	| not (pred st.s.[st.idx])
		-> ([], [])
	| isJust st.forbidden && not (isEmpty (fst (runParser (fromJust st.forbidden) {st & forbidden = ?None})))
		-> ([], [])
		-> ([(st.s.[st.idx], {st & idx=st.idx+1})], [])

pToken :: t -> Parser arr t st t | Array arr t & == t
pToken c = pSatisfy (\x -> x == c)

pPeek :: Parser arr t st [t] | Array arr t
pPeek = Parser \st -> ([([st.s.[i] \\ i <- [st.idx..size st.s-1]], st)], [])

(<?|>) infixr 4 :: (Parser arr t st a) (Parser arr t st a) -> Parser arr t st a
(<?|>) l r = Parser \st
	# (rs1,es1) = runParser l st
	  (rs2,es2) = runParser r st
	-> (rs1++rs2, es1++es2)

(<<|>) infixr 4 :: (Parser arr t st a) (Parser arr t st a) -> Parser arr t st a
(<<|>) pl pr = Parser \st -> case runParser pl st of
	([], _) -> runParser pr st
	r -> r

pOptional :: !(Parser arr t st a) -> Parser arr t st (?a)
pOptional v = (?Just <$> v) <?|> pure ?None

pFastOptional :: !(Parser arr t st a) -> Parser arr t st (?a)
pFastOptional v = (?Just <$> v) <<|> pure ?None

(<:>) infixr 6 :: (Parser arr t st a) (Parser arr t st [a]) -> Parser arr t st [a]
(<:>) p1 p2 = liftA2 (\x xs -> [x:xs]) p1 p2

pMany :: (Parser arr t st a) -> Parser arr t st [a]
pMany p = pSome p <<|> pure []

pSome :: (Parser arr t st a) -> Parser arr t st [a]
pSome p = p <:> pMany p

pSepBy :: (Parser arr t st a) (Parser arr t st s) -> Parser arr t st [a]
pSepBy pa psep = pSepBy1 pa psep <?|> pure []

pSepBy1 :: (Parser arr t st a) (Parser arr t st s) -> Parser arr t st [a]
pSepBy1 pa psep = pa <:> pMany (psep >>| pa)

pEof :: Parser arr t st [a] | Array arr t
pEof = pPeek >>= \stream -> if (isEmpty stream) (pure []) (pError (IllFormed "expected EOF"))

pRewind :: Parser arr t st ()
pRewind = Parser \st
	| st.idx == 0
		-> ([], [])
		-> ([((), {st & idx=st.idx-1})], [])

pReject :: !(Parser arr t st a) -> Parser arr t st ()
pReject rejected = Parser \st -> case runParser rejected st of
	([], _) -> ([((), st)], [])
	_       -> ([], [])

pCheck :: !(Parser arr t st a) -> Parser arr t st ()
pCheck accepted = Parser \st -> case runParser accepted st of
	([_:_], _) -> ([((), st)], [])
	_          -> ([], [])

(`forbidding`) infix :: !(Parser arr t st a) !(Parser arr t st forbidden) -> Parser arr t st a
(`forbidding`) parser forbiddenParser = Parser try
where
	try st = appFst
		(map (appSnd \st` -> {st` & forbidden = st.forbidden}))
		(runParser parser {st & forbidden = ?Just (forbiddenParser $> ())})

pNonEmpty :: !(Parser arr t st a) -> Parser arr t st a
pNonEmpty p = Parser \st -> appFst (filter (hasMoved st o snd)) $ runParser p st
where
	hasMoved org new = new.idx > org.idx

pStartOfLine :: StringParser st ()
pStartOfLine = Parser \st
	| st.idx == 0 || st.s.[st.idx-1] == '\n' || st.s.[st.idx-1] == '\r'
		-> ([((), st)], [])
		-> ([], [])
