implementation module Text.YAML.Serialize

/**
 * Copyright 2021 Camil Staps.
 *
 * This file is part of clean-yaml.
 *
 * Clean-yaml is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Clean-yaml is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * clean-yaml. If not, see <https://www.gnu.org/licenses/>.
 */

import StdEnv

import Data.List

import Text.YAML.Compose
import Text.YAML.Parse

serializeYAMLStream :: ![YAMLNode] -> [YAMLEvent]
serializeYAMLStream docs = concatMap serializeYAMLDocument docs

serializeYAMLDocument :: !YAMLNode -> [YAMLEvent]
serializeYAMLDocument doc =
	[EnterDocument False:serializeYAMLGraph doc ++ [ExitDocument False]]

serializeYAMLGraph :: !YAMLNode -> [YAMLEvent]
serializeYAMLGraph {properties,content} =
	serializeProperties properties ++ serializeContent content

serializeProperties :: !YAMLNodeProperties -> [YAMLEvent]
serializeProperties props = [PropertiesEvent props] // TODO only when necessary

serializeContent :: !YAMLNodeContent -> [YAMLEvent]
serializeContent (YAMLScalar t s) =
	[ScalarEvent t s]
serializeContent (YAMLSequence xs) =
	[EnterSequence:concatMap serializeYAMLGraph xs ++ [ExitSequence]]
serializeContent (YAMLMapping kvs) =
	[EnterMapping:concatMap serializeKeyValuePair kvs ++ [ExitMapping]]
serializeContent (YAMLAlias anchor) =
	[AliasEvent anchor]

serializeKeyValuePair :: !YAMLKeyValuePair -> [YAMLEvent]
serializeKeyValuePair {key,value} =
	serializeYAMLGraph key ++ serializeYAMLGraph value
