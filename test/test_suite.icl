module test_suite

/**
 * Copyright 2021 Camil Staps.
 *
 * This file is part of clean-yaml.
 *
 * Clean-yaml is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Clean-yaml is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * clean-yaml. If not, see <https://www.gnu.org/licenses/>.
 */

import Control.Monad
import Data.Error
import Data.Func
import Data.Functor
import Data.List
import Data.Maybe
import Data.Tuple
import StdEnv
import StdOverloadedList
import System.Directory
import System.File
import System.FilePath
import System.SysCall
import Testing.TestEvents
import qualified Text
from Text import class Text, instance Text String
import Text.GenJSON
import Text.GenPrint

import Text.YAML
import Text.YAML.Compose
import Text.YAML.JSON
import Text.YAML.Parse

derive gPrint YAMLEvent, YAMLError, YAMLNodeProperties, YAMLScalarType

Start w = runTests "yaml-test-suite" w

runTests :: !String !*World -> *World
runTests dir w
	# (mbTests,w) = readDirectory dir w
	  tests = fromOk mbTests
	| isError mbTests = abort "failed to read test directory\n"
	# w = runTests testParser tests w
	# w = runTests testToJSON tests w
	= w
where
	runTests f = seqSt (runTest f dir) o sort o filter (not o isExcluded)
	where
		isExcluded = flip IsMember excluded_directories

		runTest f dir test w
			# (mbFiles,w) = readDirectory (dir </> test) w
			  files = fromOk mbFiles
			| isError mbFiles = abort "failed to read directory\n"
			| isMember "===" files = withTitle f dir test w
			| otherwise = seqSt (\sub -> withTitle f dir (test </> sub)) (sort (removeMembers files [".", ".."])) w

		withTitle f dir test w
			| IsMember test excluded_tests
				= w
			# (mbTitle,w) = readFile (dir </> test </> "===") w
			  title = fromOk mbTitle
			| isError mbTitle = abort "failed to read title\n"
			# (hasError,w) = fileExists (dir </> test </> "error") w
			# (yamlInput,w) = mbReadFile (dir </> test </> "in.yaml") w
			# (parseEvents,w) = mbReadFile (dir </> test </> "test.event") w
			# (jsonInput,w) = mbReadFile (dir </> test </> "in.json") w
			= f
				('Text'.concat3 test ": " ('Text'.trim title))
				hasError yamlInput parseEvents jsonInput
				w

		mbReadFile path w = appFst error2mb (readFile path w)

excluded_directories =:
	[#"."
	, ".."
	, ".git"
	, "meta"
	, "name"
	, "tags"
	!]
excluded_tests =:
	[#"JEF9/02" // https://github.com/yaml/yaml-test-suite/issues/118
	, "L24T/01" // https://github.com/yaml/yaml-test-suite/issues/118
	]

log :: !String !*World -> *World
log s w
	# (io,w) = stdio w
	# io = io <<< s <<< "\n"
	# (_,w) = fclose io w
	= w

startTest :: !(!String, !String) -> *World -> *World
startTest (mod,name) = log $ toString $ toJSON $ StartEvent
	{ StartEvent
	| name     = name
	, location = ?Just {moduleName = ?Just mod}
	}

finishTest :: !(!String, !String) !EndEventType -> *World -> *World
finishTest (mod,name) event = log $ toString $ toJSON $ EndEvent
	{ name     = name
	, location = ?Just {moduleName = ?Just mod}
	, event    = event
	, message  = ""
	, time     = ?None
	}

testParser :: !String !Bool !(?String) !(?String) !(?String) !*World -> *World
testParser title expectError (?Just yaml) (?Just events) _ w
	# w = startTest test w
	= case ignoreWarnings <$> parseYAMLStream yaml of
		Error e ->
			handleError expectError test e w
		Ok parsedYAML
			| expectError ->
				finishTest test (Failed $ ?Just $ CustomFailReason "Expected parse failure") w
			# parsedEvents = parseYAMLEvents ('Text'.split "\n" events)
			| parsedYAML == parsedEvents ->
				finishTest test Passed w
			| otherwise ->
				finishTest test
					(Failed $ ?Just $ FailedAssertions [ExpectedRelation (GPrint (printToString parsedEvents)) Eq (GPrint (printToString parsedYAML))])
					w
where
	test = ("Text.YAML.Parse", "parse " +++ title)
testParser _ _ _ _ _ w
	= w

testToJSON :: !String !Bool !(?String) !(?String) !(?String) !*World -> *World
testToJSON title expectError (?Just yaml) _ (?Just json) w
	# w = startTest test w
	= case ignoreWarnings <$> parseYAMLStream yaml >>= composeYAMLGraphs of
		Error e ->
			handleError expectError test e w
		Ok [yamlNode]
			# json = fromString json
			| json=:JSONError ->
				finishTest test (Failed $ ?Just $ CustomFailReason "Unexpected JSON parse failure") w
			# mbYAMLasJSON = yamlToJSON coreSchema yamlNode
			| isError mbYAMLasJSON ->
				finishTest test (Failed $ ?Just $ CustomFailReason $ "Unexpeced error: " +++ printToString (fromError mbYAMLasJSON)) w
			# yamlAsJSON = fromOk mbYAMLasJSON
			| yamlAsJSON == json ->
				finishTest test Passed w
			| otherwise ->
				finishTest test (Failed $ ?Just $ FailedAssertions [ExpectedRelation (JSON json) Eq (JSON yamlAsJSON)]) w
		Ok _ ->
			finishTest test Skipped w // the JSON parser in Platform cannot handle more than one document
where
	test = ("Text.YAML.JSON", "toJSON " +++ title)
testToJSON _ _ _ _ _ w
	= w

handleError :: !Bool !(!String, !String) !YAMLError !*World -> *World
handleError expectError test error w
	| expectError
		= finishTest test Passed w
	| error=:(UnsupportedSyntax "only YAML 1.2+ is supported")
		= finishTest test Skipped w
	| otherwise
		= finishTest test (Failed $ ?Just $ CustomFailReason $ "Unexpected errors: " +++ printToString error) w

ignoreWarnings :: ![YAMLEvent] -> [YAMLEvent]
ignoreWarnings es = filter (\e -> not (e=:WarnEvent _)) es

parseYAMLEvents :: ([String] -> [YAMLEvent])
parseYAMLEvents = concatMap (parse o fromString)
where
	// The format does not seem to be documented anywhere, but is relatively simple:
	// https://github.com/yaml/libyaml/blob/master/tests/run-parser-test-suite.c
	parse ['=VAL ':val]
		# (anchor,tag,val) = parseAnchorAndTag val
		# scalar = parseScalarEvent val
		| isNone anchor && isNone tag
			= [scalar]
			# tag = fromMaybe (if (scalar=:(ScalarEvent Plain _)) "?" "!") tag
			= [PropertiesEvent {tag = tag, anchor = anchor}, scalar]
	parse ['+SEQ []'] = [EnterSequence]
	parse ['+SEQ [] ':properties] = [parsePropertiesEvent properties, EnterSequence]
	parse ['+SEQ'] = [EnterSequence]
	parse ['+SEQ ':properties] = [parsePropertiesEvent properties, EnterSequence]
	parse ['-SEQ'] = [ExitSequence]
	parse ['+MAP {}'] = [EnterMapping]
	parse ['+MAP {} ':properties] = [parsePropertiesEvent properties, EnterMapping]
	parse ['+MAP'] = [EnterMapping]
	parse ['+MAP ':properties] = [parsePropertiesEvent properties, EnterMapping]
	parse ['-MAP'] = [ExitMapping]
	parse ['=ALI *':name] = [AliasEvent (toString name)]
	parse ['+DOC'] = [EnterDocument False]
	parse ['+DOC ---'] = [EnterDocument True]
	parse ['-DOC'] = [ExitDocument False]
	parse ['-DOC ...'] = [ExitDocument True]
	parse ['+STR'] = []
	parse ['-STR'] = []
	parse [] = []
	parse cs = abort ("parseYAMLEvents: unknown event '"+++toString cs+++"'\n")

	parsePropertiesEvent cs
		# (anchor,tag,remainder) = parseAnchorAndTag cs
		| isNone anchor && isNone tag
			= abort "expected anchor or tag\n"
			= PropertiesEvent {tag = fromMaybe "?" tag, anchor = anchor}

	parseAnchorAndTag ['&':anchor_and_content] =
		let
			(anchor,rest) = span (not o isSpace) anchor_and_content
			(_,tag,remainder) = parseAnchorAndTag rest
		in
		(?Just (toString anchor), tag, remainder)
	parseAnchorAndTag ['<':tag_and_content] =
		let
			(tag,[_:rest]) = span ((<>) '>') tag_and_content
			(anchor,_,remainder) = parseAnchorAndTag rest
		in
		(anchor, ?Just (toString tag), remainder)
	parseAnchorAndTag [' ':rest] =
		parseAnchorAndTag rest
	parseAnchorAndTag rest =
		(?None, ?None, rest)

	parseScalarEvent [type:content] = ScalarEvent (toType type) (toString (unescape content))
	where
		toType ':' = Plain
		toType '"' = DoubleQuoted
		toType '\'' = SingleQuoted
		toType '|' = Literal
		toType '>' = Folded

		unescape ['\\':c:rest] = case c of
			'\\' -> ['\\':unescape rest]
			'0' -> ['\0':unescape rest]
			'b' -> ['\b':unescape rest]
			'n' -> ['\n':unescape rest]
			'r' -> ['\r':unescape rest]
			't' -> ['\t':unescape rest]
			_ -> abort "unknown escape sequence\n"
		unescape [c:rest] = [c:unescape rest]
		unescape [] = []
