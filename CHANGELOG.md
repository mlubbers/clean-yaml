# Changelog

## v2.0

- Feature: provide construction instances for `Map` and `Set` from containers.
- Feature: add `Dynamic` argument to `constructFromYAML` and the underlying
  class and generic. This dynamic is not used by the library itself and will be
  `()` when `constructFromYAML` is called internally, but may be used in custom
  construction instances to pass extra arguments. For example, it may be used
  to pass a version number of the data file format, so that instances can
  implement different construction logics depending on the version.
- Enhancement: `gConstructFromYAML` has been replaced by a class
  `ConstructFromYAML`. Instances of this class can be derived with the same
  behavior with `instance ConstructFromYAML MyType derive gConstructFromYAML`.
  The benefit of this approach is that instances can have additional context,
  such as `instance ConstructFromYAML (Set a) | <, ConstructFromYAML a`.

### v1.3.0

- Chore: update to containers 2, json 1/3, system 2, and text 2.

### v1.2.0

- Chore: update to base 2.0.

#### v1.1.2

- Enhancement: improve error informativeness, e.g. with unexpected `%YAML`
  directives.
- Fix: accept backslash followed by literal horizontal tab in scalars.
- Fix: reject explicit scalar indentation of `0`.
- Fix: fix parsing of block headers where the chomping indicator comes before
  the indentation indicator.
- Fix: fix indentation detection in scalars containing only whitespace.

#### v1.1.1

- Chore: move to https://clean-lang.org.

### v1.1.0

- Feature: add `constructFromScalar`, a utility function to write custom
  `gConstructFromYAML` instances.
- Change: the `RECORD` instance of `gConstructFromYAML` now inserts nodes with
  the empty tag `""` for unspecified fields, instead of using the tag `"?"`.
  This means instances can distinguish between the case that a field is
  unspecified and the case that it is specified with empty content, `""`.
- Change: the `?` instance of `gConstructFromYAML` now constructs `?None`
  instead of `?Just ?None` for unspecified record fields of type `?(?a)`.
- Fix: the `String` instance of `gConstructFromYAML` now accepts values that
  map to the `null` tag.

## v1.0.0

First tagged version.
